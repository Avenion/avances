﻿using ProyectoBanco.Inicio;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ProyectoBanco
{
    public partial class frmLogin : Form
    {
        frmInicio2 Logeo;
        frmLogin2 pestaña2;
        public frmLogin()
        {
            InitializeComponent();
        }

        private void btnIniciar_Click(object sender, EventArgs e)
        {
            if (txtUser.Text == "Admin" && txtPass.Text == "EquipoX")
            {
                MessageBox.Show("Se ha iniciado sesion!..");
                Logeo = new frmInicio2();
                Logeo.Show();
                this.Hide();
            }
            else
            {
                MessageBox.Show("Error el User o la contraseña son incorrectas..");
                txtUser.Text = "";
                txtPass.Text = "";
                txtUser.Focus();
            }
        }

        private void btnSalir_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btnUsuario_Click(object sender, EventArgs e)
        {
           MessageBox.Show("Iniciando Sesion como Usuario");
            pestaña2 = new frmLogin2();
            pestaña2.Show();
            this.Hide();
        }
    }
}
