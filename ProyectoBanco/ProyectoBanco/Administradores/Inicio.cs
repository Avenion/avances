﻿using ProyectoBanco.Inicio.Dato;
using ProyectoBanco.Inicio.Modelo;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ProyectoBanco.Inicio
{
    public partial class frmInicio2 : Form
    {
        frmLogin Admin2;
        DataTable table;
        Usuario dato = new Usuario();
        public frmInicio2()
        {
            InitializeComponent();
        }

        private void btnGuardar_Click(object sender, EventArgs e)
        {
            Guardar();
            Iniciar();
            Limpiar();
            Consultar();
        }

        private void btnLimpiar_Click(object sender, EventArgs e)
        {
            Limpiar();
        }
        private void Iniciar()
        {
            table = new DataTable();
            table.Columns.Add("Nombre");
            table.Columns.Add("Edad");
            table.Columns.Add("Clave");
            Grille.DataSource = table;
        }
        private void Guardar()
        {
            var modelo = new UsuarioModel()
            {
                Nombre = txtNombre.Text,
                Edad = int.Parse(txtEdad.Text),
                Clave = int.Parse(txtClave.Text)
            };
            dato.Guardar(modelo);
        }
        /// <summary>
        /// Aqui consultamos los datos requeridos
        /// </summary>
        private void Consultar()
        {
            foreach (var item in dato.Consultar())
            {
                DataRow fila = table.NewRow();
                fila["Nombre"] = item.Nombre;
                fila["Edad"] = item.Edad;
                fila["Clave"] = item.Clave;
                table.Rows.Add(fila);
            }
        }
        private void Limpiar()
        {
            txtNombre.Text = "";
            txtEdad.Text = "";
            txtClave.Text = "";

        }

        private void btnAtras_Click(object sender, EventArgs e)
        {
            MessageBox.Show("Volviendo a la anterior ventana");
            Admin2 = new frmLogin();
            Admin2.Show();
            this.Hide();
        }
    }
}

