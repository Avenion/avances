﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProyectoBanco.Inicio.Modelo
{
    public class UsuarioModel
    {
        public string Nombre { get; set; }

        public int Edad { get; set; }

        public int Clave { get; set; }

    }
}
