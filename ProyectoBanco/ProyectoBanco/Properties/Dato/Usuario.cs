﻿using ProyectoBanco.Inicio.Modelo;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProyectoBanco.Inicio.Dato
{
    public class Usuario
    {
        List<UsuarioModel> lista = new List<UsuarioModel>();
        /// <summary>
        /// Guarda a los usuarios
        /// </summary>
        /// <param name="modelo"> datos del usuario/param>
        public void Guardar(UsuarioModel modelo)
        {
            lista.Add(modelo);
        }
        /// <summary>
        /// Consulta todos los ususarios 
        /// </summary>
        /// <returns>datos de usuario</returns>
        public List<UsuarioModel> Consultar()
        {
            return lista;
        }
    }
}
