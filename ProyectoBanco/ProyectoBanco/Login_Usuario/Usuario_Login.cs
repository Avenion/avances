﻿using ProyectoBanco.Inicio;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ProyectoBanco
{
    public partial class frmLogin2 : Form
    {
        frmInicio2 Logeo;
        frmLogin admin;
        public frmLogin2()
        {
            InitializeComponent();
        }
        /// <summary>
        /// Aqui añadimos al Usuario de prueba
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnIngresar_Click(object sender, EventArgs e)
        {
            if (txtUser2.Text == "Usuario20" && txtPass2.Text == "123")
            {
                MessageBox.Show("Se ha iniciado sesion!..");
                Logeo = new frmInicio2();
                Logeo.Show();
                this.Hide();
            }
            else
            {
                MessageBox.Show("Error el User o la contraseña son incorrectas..");
                txtUser2.Text = "";
                txtPass2.Text = "";
                txtUser2.Focus();
            }
        }
        /// <summary>
        /// Este es el boton para salir de la pantalla
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>

        private void btnSalir2_Click(object sender, EventArgs e)
        {
            this.Close();
        }
        /// <summary>
        /// Añadimos un boton como en la pantalla de los administradores para devolvernos si no soy Usuario
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>

        private void btnAdmin_Click(object sender, EventArgs e)
        {
            MessageBox.Show("Iniciando Sesion como Administrador");
            admin = new frmLogin();
            admin.Show();
            this.Hide();
        }
    }
}

